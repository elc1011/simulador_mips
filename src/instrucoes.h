#ifndef INSTRUCOES_H
#define INSTRUCOES_H

#include "stdint.h"

extern uint32_t numero_instrucoes_executadas;

void instrucao_nao_implementada(void);
void executa_instrucao_jr(void);
void executa_instrucao_syscall(void);
void executa_instrucao_add(void);
void executa_instrucao_addu(void);
void executa_instrucao_tipo_R(void);
void executa_instrucao_j(void);
void executa_instrucao_tipo_jal(void);
void executa_instrucao_bne(void);
void executa_instrucao_addi();
void executa_instrucao_addiu();
void executa_instrucao_lui(void);
void executa_instrucao_ori(void);
void executa_instrucao_mul(void);
void executa_instrucao_1c(void);
void executa_instrucao_lw(void);
void executa_instrucao_sw(void);


#endif // INSTRUCOES_H
