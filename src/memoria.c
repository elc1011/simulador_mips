#include "memoria.h"
#include "util.h"


uint8_t memoria_instrucoes[TAM_MEMORIA_INSTRUCOES]; /**< segmento de memória para as instruções */
uint8_t memoria_dados[TAM_MEMORIA_DADOS]; /**< segmento de memória para os dados estáticos */
uint8_t memoria_pilha[TAM_MEMORIA_PILHA]; /**< segmento de memória para a pilha */

/******************************************************************************/
int endereco_pertence_segmento_memoria ( uint32_t endereco,
        uint32_t endereco_inicial,
        uint32_t endereco_final )
/******************************************************************************/
{
    if ( ( endereco>=endereco_inicial ) && ( endereco<=endereco_final ) ) {
        return 1;
    } else {
        return 0;
    }
}


/******************************************************************************/
void escreve_memoria ( uint32_t endereco, uint8_t dado )
/******************************************************************************/
{
    uint32_t indice;
    char buffer[256];

    // verifica se o endereço pertence a um segmento de memória. Se pertence, escreve.
    if ( endereco_pertence_segmento_memoria ( endereco, EI_MEMORIA_INSTRUCOES, EF_MEMORIA_INSTRUCOES ) ) {
        indice  = endereco - EI_MEMORIA_INSTRUCOES;
        memoria_instrucoes[indice] = dado;
    } else if ( endereco_pertence_segmento_memoria ( endereco, EI_MEMORIA_DADOS, EF_MEMORIA_DADOS ) ) {
        indice  = endereco - EI_MEMORIA_DADOS;
        memoria_dados[indice] = dado;
    } else if ( endereco_pertence_segmento_memoria ( endereco, EI_MEMORIA_PILHA, EF_MEMORIA_PILHA ) ) {
        indice  = endereco - EI_MEMORIA_PILHA;
        memoria_pilha[indice] = dado;
    } else {
        // erro: o segmento não foi definido
        snprintf ( buffer, sizeof buffer, "Endereço 0x%08X fora da faixa do simulador\n", endereco );
        imprime_string ( buffer );
    }
}


/******************************************************************************/
uint8_t leia_memoria ( uint32_t endereco )
/******************************************************************************/
{
    uint32_t dado;
    uint32_t indice;
    char buffer[256];

    // verifica se o endereço pertence a um segmento de memória. Se pertence, escreve.
    if ( endereco_pertence_segmento_memoria ( endereco, EI_MEMORIA_INSTRUCOES, EF_MEMORIA_INSTRUCOES ) ) {
        indice = endereco - EI_MEMORIA_INSTRUCOES;
        dado = memoria_instrucoes[indice];
    } else if ( endereco_pertence_segmento_memoria ( endereco, EI_MEMORIA_DADOS, EF_MEMORIA_DADOS ) ) {
        indice = endereco - EI_MEMORIA_DADOS;
        dado = memoria_dados[indice];
    } else if ( endereco_pertence_segmento_memoria ( endereco, EI_MEMORIA_PILHA, EF_MEMORIA_PILHA ) ) {
        indice = endereco - EI_MEMORIA_PILHA;
        dado = memoria_pilha[indice];
    } else {
        // erro: o segmento não foi definido
        snprintf ( buffer, sizeof buffer, "Endereço 0x%08X fora da faixa do simulador\n", endereco );
        imprime_string ( buffer );
        dado = 0;
    }
    return dado;
}

/******************************************************************************/
int leia_arquivo_para_memoria ( char* nome_arquivo, uint32_t endereco_inicial )
/******************************************************************************/
{

    uint32_t endereco;
    int erro;

    endereco = endereco_inicial;

    FILE *fp = fopen ( nome_arquivo, "rb" );

    if ( fp == NULL ) {
        imprime_string ( "O arquivo não pode ser aberto\n" );
        erro = 1;
    } else {
        int c;
        while ( (c = fgetc ( fp ) ) != EOF ) {
            escreve_memoria ( endereco, c );
            endereco++;
        }
        if (  (erro = ferror ( fp ))  ) {
            imprime_string ( "Erro na leitura do arquivo\n" );
        } else {
            //imprime_string ( "Arquivo lido com sucesso\n" );
        }
    }
    return (erro == 0);
}



