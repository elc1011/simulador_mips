#include "registradores.h"

// registradores de uso geral
uint32_t registradores[NUMERO_REGISTRADORES]; /**< Banco de registradores do simulador */

// registradores específicos
uint32_t hi; /**< registrador hi usados na multiplicação e divisão */
uint32_t lo; /**< registrador lo usados na multiplicação e divisão */
uint32_t PC; /**< Contador de Programa */
uint32_t IR; /**< Registrador de instruções */

// campos do registrador IR, usados na decodificação e execução
uint32_t IR_campo_op; /**< campo op: opcode da instrução */
uint32_t IR_campo_rs; /**< campo registrador rs */
uint32_t IR_campo_rt; /**< campo registrador rt */
uint32_t IR_campo_rd; /**< campo registrador rd */
uint32_t IR_campo_shamt; /** campo shamt - usado em operações de delocamento */
uint32_t IR_campo_funct; /**< campo funct, usado na decodificação de instruções tipo R */
uint32_t IR_campo_imm; /**< campo com um valor imediato */
uint32_t IR_campo_j; /**< campo com o valor imediato para instruções */

