/*  This file is part of "ELC1119 Simulador MIPS" program.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>     */

/* Projet     : ELC1119 Simulador MIPS
 * Author     : Giovani Baratto (Giovani.Baratto@ufsm.br)
 * Company    : Universiade Federal de Santa Maria
 *              Departamento de Eletrônica e Computação
 * Created    : 2020.06.22
 * Last Update: 2020.06.23
 * Version    : 0.1 (alfa)                                                    */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"
#include "memoria.h"
#include "registradores.h"
#include "instrucoes.h"
#include "servicos.h"


/**
* \brief Escreve uma mensagem apresentando o programa
*/
/******************************************************************************/
void introducao(void)
/******************************************************************************/
{
    const char *str_introducao = "\n"
                                 "**************************************\n"
                                 "Este programa simula uma máquina MIPS.\n"
                                 "**************************************\n"
                                 "\n"
                                 "\n";

    imprime_string((char *)str_introducao);
}


/**
* \brief busca uma instrução da memória e armazena no registrador IR
*
* Faz a leitura de uma instrução da memória do simulador para o registrador de
* instruções IR. O tamanho da instrução é 4 bytes. O endereço inicial da instrução
* é dado pelo registrador PC.
*/
/******************************************************************************/
void busca_instrucao(void)
/******************************************************************************/
{
    IR = leia_memoria(PC) | leia_memoria(PC + 1) << 8 |
         leia_memoria(PC + 2) << 16 | leia_memoria(PC + 3) << 24;
}


/**
* \brief decodifica a instrução do registrador IR
*
* Duas tarefas são realizadas por este procedimento:
* (1) Todos os campos são encontrados da instrução IR, pois no momento da
* decodificação, não conhecemos a instrução neste registrador.
* (2) O endereço em PC é incrementado para apontar para a próxima instrução.
*/
/******************************************************************************/
void decodifica_instrucao(void)
/******************************************************************************/
{
    IR_campo_op = IR >> 26;
    IR_campo_rs = (IR >> 21) & 0x0000001F;
    IR_campo_rt = (IR >> 16) & 0x0000001F;
    IR_campo_rd = (IR >> 11) & 0x0000001F;
    IR_campo_shamt = (IR >> 6) & 0x0000001F;
    IR_campo_funct = IR & 0x0000003F;
    IR_campo_imm = IR & 0x0000FFFF;
    /* extendemos o sinal do campo com o valor imediato. O valor imediato é
     * convertido de 16 bits para 32 bits. Verificamos o bit mais significativo
     * do valor imediato de 16 bits. Este bit é replicado para os bits mais
     * siginificativos (bits 16 a 31 do campo IR_campo_imm
     */
    if (IR & 0x00008000) { // extende o sinal do número
        IR_campo_imm |= 0xFFFF0000;
    }
    IR_campo_j = IR & 0x03FFFFFF;
    PC += 4;
}


/**
 * \brief O simulador executa a instrução do registrador IR
 *
 * O campo op é verificado e a instrução é executada. Se o campo op é igual a
 * 0, temos uma instrução tipo R. Neste caso, a instrução é executada após a
 * análise do campo funct. Dependendo do valor de op, chamamos o correspondente
 * procedimento para executar a instrução no simulador.
 */
/******************************************************************************/
void executa_instrucao(void)
/******************************************************************************/
{

    switch (IR_campo_op) {
    case 0x00:
        executa_instrucao_tipo_R();
        break;
    case 0x02:
        executa_instrucao_j();
        break;
    case 0x03:
        executa_instrucao_tipo_jal();
        break;
    case 0x05:
        executa_instrucao_bne();
        break;
    case 0x08:
        executa_instrucao_addi();
        break;
    case 0x09:
        executa_instrucao_addiu();
        break;
    case 0x0F:
        executa_instrucao_lui();
        break;
    case 0x0D:
        executa_instrucao_ori();
        break;
    case 0x1C:
        executa_instrucao_1c();
        break;
    case 0x23:
        executa_instrucao_lw();
        break;
    case 0x2B:
        executa_instrucao_sw();
        break;
    default:
        instrucao_nao_implementada();
    }
    numero_instrucoes_executadas++;
}

/**
* \brief Inicializa alguns registradores e variáveis com os valores padrões
*/
/******************************************************************************/
void inicializa_registradores_variaveis()
/******************************************************************************/
{
    PC = PC_DEFAULT;
    registradores[$gp] = GP_DEFAULT;
    registradores[$sp] = SP_DEFAULT;
    executou_servico_exit = 0;
    numero_instrucoes_executadas = 0;
}

/**
*  \brief Carrega os arquivos text.bin e data.bin para a memória do simulador
*/
/******************************************************************************/
void carrega_programa(void)
/******************************************************************************/
{
    const char *nome_arquivo_programa = "text.bin";
    const char *nome_arquivo_dados = "data.bin";
    int ok;

    ok = leia_arquivo_para_memoria((char *)nome_arquivo_programa, EI_MEMORIA_INSTRUCOES);
    if(!ok) exit (EXIT_FAILURE);
    leia_arquivo_para_memoria((char *)nome_arquivo_dados, EI_MEMORIA_DADOS);
    if(!ok) exit (EXIT_FAILURE);
}

/**
* \brief Apresenta uma mensagem avisando que a simulação terminou.
*/
/******************************************************************************/
void termina_simulacao(void)
/******************************************************************************/
{
    const char *str_introducao = "\n"
                                 "**************************************\n"
                                 "A simulação terminou.\n"
                                 "**************************************\n"
                                 "\n"
                                 "\n";

    imprime_string((char *)str_introducao);
}


/**
* \brief Programa que realiza a simulação do processador MIPS executando um programa
*/
/******************************************************************************/
int main(void)
/******************************************************************************/
{
    introducao();
    carrega_programa();
    inicializa_registradores_variaveis();
    while (!executou_servico_exit) {
        busca_instrucao();
        decodifica_instrucao();
        executa_instrucao();
    }
    termina_simulacao();
    return 0;
}
