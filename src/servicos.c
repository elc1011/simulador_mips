#include "util.h"
#include "memoria.h"
#include "registradores.h"
#include "servicos.h"

int executou_servico_exit;

// imprime um inteiro
/***********************************************************/
void executa_servico_imprime_inteiro(void)
/***********************************************************/
{
    char buffer[16];
    snprintf(buffer, sizeof buffer, "%d", registradores[$a0]);
    imprime_string(buffer);
}

/***********************************************************/
void executa_servico_imprime_string(void)
/***********************************************************/
{
    uint32_t endereco_caractere = registradores[$a0];
    uint32_t caractere_string;

    while ((caractere_string = leia_memoria(endereco_caractere)) != 0) {
        escreve_caractere(caractere_string);
        endereco_caractere++;
    }
}

/***********************************************************/
void executa_servico_exit(void)
/***********************************************************/
{
    executou_servico_exit = 1;
}

/***********************************************************/
void executa_servico_imprime_caractere(void)
/***********************************************************/
{
    escreve_caractere(registradores[$a0]);
}
