#ifndef REGISTRADORES_H_INCLUDED
#define REGISTRADORES_H_INCLUDED

#include <stdint.h>

// valores default para os registradores

/* Valores iniciais para os registradores do simulador */
#define PC_DEFAULT 0x00400000 /**< Valor incial do PC (program counter) */
#define SP_DEFAULT 0x7FFFEFFC /**< Valor incial do SP (stack pointer) */
#define GP_DEFAULT 0x10008000 /**< Valor incial do GP (global pointer) */

#define NUMERO_REGISTRADORES 32 /**< Número de registradores de uso geral do MIPS */

// registradores de uso geral
extern uint32_t registradores[NUMERO_REGISTRADORES]; /**< Banco de registradores do simulador */

// registradores específicos
extern uint32_t hi; /**< registrador hi usados na multiplicação e divisão */
extern uint32_t lo; /**< registrador lo usados na multiplicação e divisão */
extern uint32_t PC; /**< Contador de Programa */
extern uint32_t IR; /**< Registrador de instruções */

// campos do registrador IR, usados na decodificação e execução
extern uint32_t IR_campo_op; /**< campo op: opcode da instrução */
extern uint32_t IR_campo_rs; /**< campo registrador rs */
extern uint32_t IR_campo_rt; /**< campo registrador rt */
extern uint32_t IR_campo_rd; /**< campo registrador rd */
extern uint32_t IR_campo_shamt; /** campo shamt - usado em operações de delocamento */
extern uint32_t IR_campo_funct; /**< campo funct, usado na decodificação de instruções tipo R */
extern uint32_t IR_campo_imm; /**< campo com um valor imediato */
extern uint32_t IR_campo_j; /**< campo com o valor imediato para instruções */

enum nomes_registradores_definicao {$zero, $at, $v0, $v1, $a0, $a1, $a2, $a3, $t0, $t1, $t2,
                          $t3, $t4, $t5, $t6, $t7, $s0, $s1, $s2, $s3, $s4, $s5,
                          $s6, $s7, $t8, $t9, $k0, $k1, $gp, $sp, $fp, $ra
                         }; /**< nomes dos 32 registradores de uso geral */

extern enum nomes_registradores_definicao nomes_registradores;

#endif // REGISTRADORES_H_INCLUDED

