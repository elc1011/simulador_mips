#ifndef MEMORIA_H_INCLUDED
#define MEMORIA_H_INCLUDED

#include <stdint.h>

#define EI_MEMORIA_INSTRUCOES 0X00400000 /**< Endereço inicial da memória de instruções */
#define EF_MEMORIA_INSTRUCOES 0X0040FFFF /**< ùltimo endereço da memória de instruções */
#define TAM_MEMORIA_INSTRUCOES ((EF_MEMORIA_INSTRUCOES - EI_MEMORIA_INSTRUCOES) + 1) /**< Tamanho do segmento da memória para as instruções */

#define EI_MEMORIA_DADOS 0X10010000 /**< Endereço inicial da memória de dados */
#define EF_MEMORIA_DADOS 0X1001FFFF /**< ùltimo endereço da memória de dados */
#define TAM_MEMORIA_DADOS ((EF_MEMORIA_DADOS - EI_MEMORIA_DADOS) + 1)/**< Tamanho do segmento da memória para os dados */

#define EI_MEMORIA_PILHA 0x7FFF0000 /**< Endereço inicial da memória reservada para a pilha */
#define EF_MEMORIA_PILHA 0X7FFFFFFF /**< ùltimo endereço da memória da pilha */
#define TAM_MEMORIA_PILHA ((EF_MEMORIA_PILHA - EI_MEMORIA_PILHA) + 1) /**< Tamanho do segmento da memória para a pilha */


/** \brief Verifica se um endereço pertence a um segmento de memória do simulador.
 *
 * Este procedimento verifica se um endereço de memória pertence a um segmento de memória.
 * Se endereço estiver contido na faixa de endereços de endereco_inicial a endereco_final, o
 * procedimento retorna 1, caso contrário 0. Este procedimento verifica se endereco é maior
 * ou igual ao endereco_inicial e se o endereco é menor ou igual a endereco_inicial.
 *
 * \param endereco endereço de memória que será verificado se está no segmento entre endereco_inicial e endereco_final.
 * \param endereco_inicial Endereço inicial do segmento de memória.
 * \param endereco_final endereço final do segmento de memória.
 * \return 1 se endereco pertence ao segmento de memória senão 0. */
int endereco_pertence_segmento_memoria ( uint32_t endereco, uint32_t endereco_inicial, uint32_t endereco_final );

/** \brief Escreve um dado (byte) na memória
 *
 *  Este procedimento escreve um byte em um endereço de memória. Este procedimento verifica se o endereço está contido
 *  em um dos três segmentos de memória definidos no simulador. Se está, o dado (byte) é armazendo na variável representado
 *  este segmento. Se o endereço não estiver em nenhum dos segmentos de memória (segmento com as instruções (.text), 
 *  segmento de dados (.data) ou segmento da pilha), uma mensagem de erro é apresentada.
 *
 *  \param endereco endereço de memória.
 *  \param dado dado (byte) que será armazendo no endereço.
 * */
void escreve_memoria ( uint32_t endereco, uint8_t dado );

/** \brief Lê um dado (byte) da memória
 *
 *  Este procedimento lê um byte de um endereço de memória. Este procedimento verifica se o endereço está contido
 *  em um dos três segmentos de memória definidos no simulador. Se está, um dado (byte) é lido da variável representado
 *  este segmento. Se o endereço não estiver em nenhum dos segmentos de memória (segmento com as instruções (.text), 
 *  segmento de dados (.data) ou segmento da pilha), uma mensagem de erro é apresentada.
 *
 *  \param endereco endereço de memória.
 *  \return dado dado (byte) associado ao endereço de memória. Se o endereço não estiver em um dos segmentos do 
 *          simulador, será retornado o valor 0.
 * */
uint8_t leia_memoria ( uint32_t endereco );

/** \brief Lê um arquivo para a memória do simulador.
 *
 *  Faz a leitura do arquivo binário com o nome nome_arquivo e escreve na memória do simulador. Os bytes lidos do
 *  arquivo são armazenados na memória, començando pelo endereco_inicial.
 *
 *  \param nome_arquivo nome do arquivo que será lido
 *  \param endereco_inicial endereço inicial da memória do simulador onde os bytes do arquivo serão gravados
 *  \return 1 se a operação de leitura e escrita na memória foi realizada com
 *  sucesso, senão retorna 0.
 * */
int leia_arquivo_para_memoria ( char* nome_arquivo, uint32_t endereco_inicial );


#endif // MEMORIA_H_INCLUDED
