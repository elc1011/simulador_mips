#ifndef SERVICOS_H
#define SERVICOS_H

extern int executou_servico_exit;

void executa_servico_imprime_inteiro(void);
void executa_servico_imprime_string(void);
void executa_servico_exit(void);
void executa_servico_imprime_caractere(void);

#endif // SERVICOS_H
