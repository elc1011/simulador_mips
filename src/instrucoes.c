#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"
#include "memoria.h"
#include "registradores.h"
#include "servicos.h"
#include "instrucoes.h"

uint32_t numero_instrucoes_executadas;

/***********************************************************/
void instrucao_nao_implementada(void)
/***********************************************************/
{
    char buffer[256];

    snprintf(buffer, sizeof buffer, "Instrução não implementada -> [0x%08X]\n", IR);
    imprime_string(buffer);
}

/***********************************************************/
void executa_instrucao_jr(void)
/***********************************************************/
{
    // imprime_string ( "instrução jr\n" );
    PC = registradores[$ra];
}

/***********************************************************/
void executa_instrucao_syscall(void)
/***********************************************************/
{
    // imprime_string ( "instrução syscall\n" );
    switch (registradores[$v0]) {
    case 1:
        executa_servico_imprime_inteiro();
        break;
    case 4:
        executa_servico_imprime_string();
        break;
    case 10:
        executa_servico_exit();
        break;
    case 11:
        executa_servico_imprime_caractere();
        break;
    default:
        imprime_string("serviço não implementado\n");
    }
}

/***********************************************************/
void executa_instrucao_add(void)
/***********************************************************/
{
    // imprime_string ( "instrução add\n" );
    registradores[IR_campo_rd] =
        (int32_t)registradores[IR_campo_rs] + (int32_t)registradores[IR_campo_rt];
}

/***********************************************************/
void executa_instrucao_addu(void)
/***********************************************************/
{
    // imprime_string ( "instrução addu\n" );
    registradores[IR_campo_rd] =
        (int32_t)registradores[IR_campo_rs] + (int32_t)registradores[IR_campo_rt];
}

/***********************************************************/
void executa_instrucao_tipo_R(void)
/***********************************************************/
{
    // imprime_string ( "instrução tipo R\n" );
    switch (IR_campo_funct) {
    case 8:
        executa_instrucao_jr();
        break;
    case 12:
        executa_instrucao_syscall();
        break;
    case 32:
        executa_instrucao_add();
        break;
    case 33:
        executa_instrucao_addu();
        break;
    default:
        instrucao_nao_implementada();
    }
}

/***********************************************************/
void executa_instrucao_j(void)
/***********************************************************/
{
    // imprime_string ( "instrução j\n" );
    PC = (PC & 0xF0000000) | ((IR_campo_j << 2) & 0x0FFFFFFF);
}

/***********************************************************/
void executa_instrucao_tipo_jal(void)
/***********************************************************/
{
    // imprime_string ( "instrução jal\n" );
    registradores[31] = PC;
    PC = (PC & 0xF0000000) | ((IR_campo_j << 2) & 0x0FFFFFFF);
}

/***********************************************************/
void executa_instrucao_bne(void)
/***********************************************************/
{
    if (registradores[IR_campo_rs] != registradores[IR_campo_rt]) {
        PC = PC + (int32_t)(IR_campo_imm << 2);
    }
    // imprime_string ( "instrução bne\n" );
}

/***********************************************************/
void executa_instrucao_addi()
/***********************************************************/
{
    // imprime_string ( "instrução addi\n" );
    registradores[IR_campo_rt] =
        registradores[IR_campo_rs] + (int32_t)IR_campo_imm;
}

/***********************************************************/
void executa_instrucao_addiu()
/***********************************************************/
{
    // imprime_string ( "instrução addiu\n" );
    registradores[IR_campo_rt] =
        registradores[IR_campo_rs] + (int32_t)IR_campo_imm;
}

/***********************************************************/
void executa_instrucao_lui(void)
/***********************************************************/
{
    // imprime_string ( "instrução lui\n" );
    registradores[IR_campo_rt] = (IR_campo_imm & 0x0000FFFF) << 16;
}

/***********************************************************/
void executa_instrucao_ori(void)
/***********************************************************/
{
    // imprime_string ( "instrução ori\n" );
    registradores[IR_campo_rt] =
        registradores[IR_campo_rs] | (IR_campo_imm & 0x0000FFFF);
}

/***********************************************************/
void executa_instrucao_mul(void)
/***********************************************************/
{
    uint64_t tmp;

    tmp = (uint64_t)registradores[IR_campo_rs] *
          (uint64_t)registradores[IR_campo_rt];
    hi = (tmp >> 32);
    lo = (uint32_t)(tmp & 0x00000000FFFFFFFF);
    registradores[IR_campo_rd] = lo;
}

/**********************************/
void executa_instrucao_1c(void)
/**********************************/
{
    // imprime_string ( "instrução 1c\n" );
    switch (IR_campo_funct) {
    case 2:
        executa_instrucao_mul();
        break;
    default:
        instrucao_nao_implementada();
    }
}

/**********************************/
void executa_instrucao_lw(void)
/**********************************/
{
    uint32_t tmp;
    uint32_t endereco;

    endereco = registradores[IR_campo_rs] + (int32_t)IR_campo_imm;

    tmp = leia_memoria(endereco) | leia_memoria(endereco + 1) << 8 |
          leia_memoria(endereco + 2) << 16 | leia_memoria(endereco + 3) << 24;
    registradores[IR_campo_rt] = tmp;
    // imprime_string ( "instrução lw\n" );
}

/**********************************/
void executa_instrucao_sw(void)
/**********************************/
{
    // imprime_string ( "instrução sw\n" );
    uint32_t endereco;

    endereco = registradores[IR_campo_rs] + (int32_t)IR_campo_imm;
    escreve_memoria(endereco, (uint8_t)registradores[IR_campo_rt]);
    escreve_memoria(endereco + 1, (uint8_t)(registradores[IR_campo_rt] >> 8));
    escreve_memoria(endereco + 2, (uint8_t)(registradores[IR_campo_rt] >> 16));
    escreve_memoria(endereco + 3, (uint8_t)(registradores[IR_campo_rt] >> 24));
}
