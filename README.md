Para compilar o programa:

1. Crie um diretório build (mkdir build)
2. Entre no diretório build (cd build)
3. Execute o programa cmake (cmake ../)
3. Execute o programa make (make)

Para realizar a simulação, adicione os arquivos text.bin e data.bin no mesmo 
diretório do programa executável e execute o programa simulador (./simulador). 
Os arquivos text.bin e data.bin estão no diretório test. O programa deve apresentar
a seguinte saída:

**************************************
Este programa simula uma máquina MIPS.
**************************************


O fatorial de 5 � 120

**************************************
A simulação terminou.
**************************************

